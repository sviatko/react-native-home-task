import React from 'react';
import {TextInput} from 'react-native';

import {styles} from './styles';

const Input = ({value, onChange, inputProps}) => (
  <TextInput
    style={styles.input}
    value={value}
    onChangeText={onChange}
    {...inputProps}
  />
);

export default Input;
