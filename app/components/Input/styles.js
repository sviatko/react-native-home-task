import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  input: {
    marginTop: 18,
    marginBottom: 24,
    paddingBottom: 6,
    borderWidth: 1,
    borderColor: '#999',
    borderStyle: 'solid',
    borderRadius: 50,
    paddingLeft: 32,
    // paddingRight: 32,
  },
});
