import React from 'react';
import {View, Alert} from 'react-native';

import {styles} from './styles';
import Button from '../Button';

const ButtonGroup = ({onDelete, onCall}) => (
  <View style={styles.container}>
    <Button text={'Call'} color={'primary'} onPress={() => onCall()} />
    <Button
      text={'Delete'}
      color={'danger'}
      onPress={() =>
        Alert.alert(
          'Alert',
          'Are you sure you want to delete a contact?',
          [
            {
              text: "I'm sure",
              onPress: onDelete,
            },
            {
              text: 'Cancel',
              style: 'cancel',
            },
          ],
          {cancelable: true},
        )
      }
    />
  </View>
);

export default ButtonGroup;
