import React from 'react';
import {TouchableOpacity, Text} from 'react-native';

import {styles} from './styles';

const AddContactButton = ({onPress}) => (
  <TouchableOpacity onPress={onPress} style={styles.root}>
    <Text style={styles.text}>+</Text>
  </TouchableOpacity>
);

export default AddContactButton;
