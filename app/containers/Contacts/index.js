import React, {useEffect, useState} from 'react';
import {FlatList, View} from 'react-native';

import {styles} from './styles';
import Contact from '../../components/Contact';
import Input from '../../components/Input';
import AddContactButton from '../../components/AddContactButton';
import {
  requestForPermissions,
  getContacts,
  deleteContact,
  openContactForm,
  callTo,
} from '../../services/ContactService';

const ContactsScreen = ({navigation}) => {
  const [search, setSearch] = useState('');
  const [contacts, setContacts] = useState([]);

  useEffect(() => {
    requestForPermissions().then(() => {
      const filter = (contact) =>
        contact.displayName.toLowerCase().startsWith(search.toLowerCase());

      getContacts(setContacts, filter);
    });
  }, [search]);

  const onChange = (text) => setSearch(text);
  return (
    <View style={styles.container}>
      <Input
        value={search}
        onChange={onChange}
        inputProps={{
          placeholder: 'Search for name',
        }}
      />
      <FlatList
        data={contacts}
        renderItem={({item}) => (
          <Contact
            key={item.displayName}
            onPress={() =>
              navigation.navigate('Contact', {
                name: item.displayName,
                number: item.phoneNumbers[0].number,
                onDelete: () => {
                  deleteContact(item.recordID, () => getContacts(setContacts));
                },
                onCall: () => {
                  callTo(item.phoneNumbers[0].number);
                },
              })
            }
            name={item.displayName}
          />
        )}
      />
      <AddContactButton
        onPress={() => openContactForm(() => getContacts(setContacts))}
      />
    </View>
  );
};

export default ContactsScreen;
