import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    paddingLeft: 32,
    paddingRight: 32,
    position: 'relative',
    display: 'flex',
    flex: 1,
  },
});
