import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    paddingLeft: 32,
    paddingRight: 32,
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
  },
});
